# Arc Black Theme

A fork of the Arc Theme with grayscale theming and colored window buttons.

Arc is a flat theme with transparent elements for GTK 3, GTK 2 and various desktop
shells, window managers and applications. It's well suited for GTK based desktop
environments such as GNOME, Cinnamon, Xfce, Unity, MATE, Budgie etc.

The theme was originally designed and developed
by [horst3180](https://github.com/horst3180/arc-theme), but the project has been
unmaintained since May 2017.

This fork aims to keep the theme updated with new toolkit and desktop environment
versions, resolve pre-existing issues, and improve and polish the theme while preserving
the original visual design.

## Arc Black

![A screenshot of the Arc theme](screenshots/arc-black.png)

## Supported toolkits and desktops

Arc comes with themes for the following:

* GTK 2
* GTK 3
* GTK 4
* GNOME Shell >=3.28
* Cinnamon >=3.8
* Unity
* Metacity
* Xfwm
* Plank

## Installation

#### Manual installation

For installing the theme by compiling it from the source code,
see [INSTALL.md](INSTALL.md) for build instructions, list of dependencies, build options
and additional details.

## Full Preview

![A full screenshot of the Arc theme](screenshots/full_preview.png)
